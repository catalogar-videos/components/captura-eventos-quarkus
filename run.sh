#!/bin/bash

# Verifica se o arquivo local.env existe
if [ ! -f local.env ]; then
  echo "Arquivo local.env não encontrado."
  exit 1
fi

# Lê cada linha do arquivo local.env
while IFS= read -r line; do
  # Ignora linhas vazias ou comentários
  if [[ -z "$line" || "$line" =~ ^# ]]; then
    continue
  fi

  echo "$line"

  # Separa a chave e o valor
  key=$(echo "$line" | cut -d= -f1)
  value=$(echo "$line" | cut -d= -f2-)

  # Exporta a variável
  export "$key=$value"
done < local.env

echo "Variáveis de ambiente carregadas com sucesso!"

echo "Executando command"

./target/captura-eventos-quarkus-1.0-runner
