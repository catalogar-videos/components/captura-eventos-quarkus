# Imagem base Alpine Linux
FROM alpine:latest

# Usuário non-root
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

# Instalação de dependências
RUN apk add --no-cache gcompat openjdk21-jre

# Define o diretório de trabalho
WORKDIR /home/appuser

USER appuser

COPY target/captura-eventos-quarkus-1.0-runner app

# Comando para iniciar o aplicativo
ENTRYPOINT ["./app"]
