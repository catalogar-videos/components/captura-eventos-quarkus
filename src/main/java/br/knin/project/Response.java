package br.knin.project;


import lombok.Getter;
import lombok.Setter;

import java.util.Collection;


@Setter
@Getter
public class Response {

    private String id;

    private String event;

    private Collection<Information> infos;

    @Setter
    @Getter
    public static class Information {

        private String key;

        private String value;

    }

}
