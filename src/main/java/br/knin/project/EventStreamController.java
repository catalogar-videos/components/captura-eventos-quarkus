package br.knin.project;

import io.smallrye.mutiny.Multi;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.jboss.resteasy.reactive.RestStreamElementType;

@Path("/events")
public class EventStreamController {

    @Channel("queue.captura.event")
    Multi<Response> events;

    @GET
    @RestStreamElementType(MediaType.SERVER_SENT_EVENTS)
    public Multi<Response> stream() {
        return events;
    }

}
